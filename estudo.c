#include <stdio.h>

int main(void) {
	unsigned int num = 0x00000000;
	unsigned int mask = 0x000000FF;
	unsigned int r = 254;
	unsigned int g = 128;
	unsigned int b = 64;
	unsigned int k = 120;
	num = (r << 8 * 0) | num;
	num = (g << 8 * 1) | num;
	num = (b << 8 * 2) | num;
	num = (k << 8 * 3) | num;
	printf("%X\n", num);

	return 0;
}
