// Compile using: nvcc -arch=sm_61 --compiler-options -O2,-Wall -I/usr/local/cuda/include main.cu -o teste
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include <time.h>

// TODO: Definir o tamanho do elemento estruturante
#define STRUCT_SIDE 3
#define STRUCT_SIZE STRUCT_SIDE*STRUCT_SIDE

#define THREADS_PER_BLOCK 5

#define CHECK(call)                                                            \
{                                                                              \
    const cudaError_t error = call;                                            \
    if (error != cudaSuccess)                                                  \
    {                                                                          \
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__);                 \
        fprintf(stderr, "code: %d, reason: %s\n", error,                       \
                cudaGetErrorString(error));                                    \
        exit(1);                                                               \
    }                                                                          \
}

__global__ static void computeNewImage(int w, int h, int round, int s, unsigned int *img, unsigned int *final_image, float *weights){
    // Aceder ao índice da thread certa
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int idy = threadIdx.y + blockIdx.y * blockDim.y;

    if(idx >= h || idy >= w){ // Fora do array
      return;
    }


    //unsigned int *se = (unsigned int*)malloc(struct_size * sizeof(unsigned int));
    unsigned int se[STRUCT_SIZE];
    int struct_size = s*s;

    unsigned int temp_gray, temp_r, temp_g, temp_b;
    int x, y, temp_pos;

    for(int i = 0; i < struct_size; i++){
      // Calcular posição 2D em array 1D do ponto do elemento estruturante que nos interessa
      x = i / s + (idx - (s/2));

      if(x < 0){
        x = -x;
      }
      if(x >= h){
        x = 2 * h - 2 - x;
      }

      y = i % s + (idy - (s / 2));
      if(y < 0){
        y = -y;
      }
      if(y >= w){
        y = 2 * w - 2 - y;
      }

      temp_pos = x * w + y;

      // Pegar nos valores todos e calcular o grey para todos os indices
      temp_r = (img[temp_pos] & 0x000000FF);
      temp_g = (img[temp_pos] & 0x0000FF00) >> 8;
      temp_b = (img[temp_pos] & 0x00FF0000) >> 16;

      temp_gray = (0.299 * temp_r) + (0.587 * temp_g) + (0.114 * temp_b);

      se[i] = img[temp_pos];
      se[i] = se[i] | (temp_gray << 24);
    }

    // Retirar cantos caso necessário (de maneira a não estragar a mediana e na média ponderada os pesos já vêem a 0)
    if(round){
      se[0] = 0x00000000;
      se[s - 1] = 0x00000000;
      se[struct_size - 1] = 0xFFFFFFFF;
      se[struct_size - s] = 0xFFFFFFFF;
    }

    //Ordenar o array
    unsigned int t_swap;
    for(int i = 0; i < struct_size; i++){
      for(int j = 0; j < struct_size-1; j++){
        if (se[j] > se[j+1]){
          t_swap = se[j];
          se[j] = se[j+1];
          se[j+1] = t_swap;
        }
      }
    }

    unsigned int new_r = 0, new_g = 0, new_b = 0;
    // Multiplicar pelos pesos
    for(int b = 0; b < struct_size; b++){
       new_r = new_r + ((se[b] & 0x000000FF) * weights[b]);
       new_g = new_g + (((se[b] & 0x0000FF00) >> 8) * weights[b]);
       new_b = new_b + (((se[b] & 0x00FF0000) >> 16) * weights[b]);
    }

    // Armazenar na posição final
    final_image[idx * w + idy] = 0x00000000;
    final_image[idx * w + idy] = final_image[idx * w + idy] | new_r;
    final_image[idx * w + idy] = final_image[idx * w + idy] | (new_g << 8);
    final_image[idx * w + idy] = final_image[idx * w + idy] | (new_b << 16);
}

static double real_time(void)
{
  struct timespec t;

  if(clock_gettime(CLOCK_REALTIME,&t) != 0) // to measure CPU time only, use CLOCK_PROCESS_CPUTIME_ID instead of CLOCK_REALTIME
    return -1.0; // clock_gettime() failed so we return an illegal value
  return (double)t.tv_sec + 1.0e-9 * (double)t.tv_nsec;
}

int main( int argc, char* argv[] ){
    int width, height, size, max_value; // Dimensões
    int rounded = 0;
    unsigned int *image;
    unsigned int *final_image;

    /*
        Lêr a imagem
    */
    FILE *stream = fopen("rninet_ascii.ppm", "r"); // TODO: Definir imagem de entrada
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;

    if (stream == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    // Magic number
    nread = getline(&line, &len, stream); // Must be "P3"
    if(strcmp(line, "P3\n") != 0){
      printf("Invalid file (Wrong magic number)\n");
      return 0;
    }


    //Dimensions
    nread = getline(&line, &len, stream);

    char *token = strtok(line, " ");
    width = atoi(token);
    token = strtok(NULL, " ");
    height = atoi(token);
    size = width * height;

    printf("Image size: %u x %u totalling %u\n",width, height, size);

    // Maximum value
    nread = getline(&line, &len, stream);
    max_value = atoi(line);

    /*
        Leitura dos valores e armazenamento
    */
    image = (unsigned int *) malloc(size * sizeof(unsigned int));
    for(int j = 0; j <= size * 3; j++){
      nread = getline(&line, &len, stream);
      if(atoi(line) > 255){
        printf("ERROR: Invalid value");
        return 0;
      }
      image[j/3] = image[j/3] | (atoi(line) << (8 * (j % 3)));
    }

    free(line);
    fclose(stream);

    /*
        Definir o elemento estruturante
    */
    int side = STRUCT_SIDE; // TODO: Definir o tamanho do lado do elemento estruturante
    int struct_size = STRUCT_SIZE;
    int real_size = struct_size;
    rounded = 0;      // TODO: Definir se tem cantos
    if(rounded){
      real_size -= 4;
    }
    int p = 12;       // TODO: Definir a relação
    if(p < real_size){
      printf("ERROR: Invalid relation");
      return 0;
    }

    // Constuir os 2 arrays do elemento estruturante
    float weight_map[struct_size]; // Definir o valor de todos
    for(int i = 0; i < struct_size; i++){
      weight_map[i] = (1.0/p);
    }
    weight_map[real_size / 2] = (p - (real_size - 1.0))/ p;  // Definir como maior o valor do meio

    if(real_size == 21){ // Se o filtro não considerar os cantos basta colocar os seus pesos a 0
      weight_map[0] = 0;
      weight_map[4] = 0;
      weight_map[20] = 0;
      weight_map[25] = 0;
    }


    /*
        Enviar informação para o device
    */
    // Setup
    int dev = 0;

    cudaDeviceProp deviceProp;
    CHECK (cudaGetDeviceProperties (&deviceProp, dev));
    //printf("Using Device %d: %s\n", dev, deviceProp.name);
    CHECK (cudaSetDevice (dev));

    // Alocação
    unsigned int *d_image, *d_final_image; // Arrays com size elementos
    //int *d_width, *d_height, *d_struct_size, *d_round, *d_side;
    float *d_weight_map; // Array com struct_size elementos

    final_image = (unsigned int *) malloc(size * sizeof(unsigned int));

    CHECK(cudaMalloc((void **)&d_image, sizeof(unsigned int) * size)); // Array com imagem de entrada
    CHECK(cudaMalloc((void **)&d_final_image, sizeof(unsigned int) * size)); // Array com imagem de saída

    CHECK(cudaMalloc((void **)&d_weight_map, sizeof(float) * struct_size)); // Array com os pesos

    // Cópia para o device
    CHECK(cudaMemcpy(d_image, image, sizeof(unsigned int) * size, cudaMemcpyHostToDevice));

    CHECK(cudaMemcpy(d_weight_map, weight_map, sizeof(float) * struct_size, cudaMemcpyHostToDevice));


    // Chamar kernel para calcular novo valor de cada pixel
    dim3 gridSize((int)(height/THREADS_PER_BLOCK)+1,(int)(width/THREADS_PER_BLOCK)+1,1); // Fazer a mais e mandar retornar os fora dos limites
    dim3 blockSize(THREADS_PER_BLOCK,THREADS_PER_BLOCK,1);
    double t0, t1;

    for(int i = 1; i < 32; i++){
      gridSize.x = (int)(height/i)+1;
      gridSize.y = (int)(width/i)+1;
      gridSize.z = 1;
      blockSize.x = i;
      blockSize.y = i;
      blockSize.z = 1;
      t0 = real_time();
      computeNewImage<<<gridSize,blockSize>>>(width,height, rounded, side, d_image, d_final_image, d_weight_map);
      CHECK(cudaDeviceSynchronize ());                            // wait for kernel to finish
      CHECK(cudaGetLastError ());                                 // check for kernel errors
      t1 = real_time();
      printf ("Time required with %d threads per block: %8.5f\n",i ,t1 - t0);
    }


    /*
        Retornar informação do device e general clean up
    */
    CHECK(cudaMemcpy(final_image, d_final_image, sizeof(unsigned int) * size, cudaMemcpyDeviceToHost));


    //printf("3: %u\n",final_image[0]);
    //cudaFree(d_rounded); cudaFree(d_width); cudaFree(d_height); cudaFree(d_struct_size);
    CHECK(cudaFree(d_image)); CHECK(cudaFree(d_final_image)); CHECK(cudaFree(d_weight_map));

    /*
        Guardar imagem
    */
    FILE *f = fopen("rninet_final.ppm", "w"); // TODO: Definir imagem de saída
    if (f == NULL){
        printf("Error opening file!\n");
        exit(1);
    }

    // Write magic number
    fprintf(f,"P3\n");

    // Write dimensions
    fprintf(f,"%u %u\n", width, height);

    // Write maximum value
    fprintf(f,"%u\n", max_value);

    // Write RGB values
    unsigned int r;
    unsigned int g;
    unsigned int b;
    for(int z = 0; z < size; z++){
      r = final_image[z] & 0x000000FF;
      g = (final_image[z] & 0x0000FF00)>>8;
      b = (final_image[z] & 0x00FF0000)>>16;
      fprintf(f,"%u\n%u\n%u\n",r,g,b);
    }
    fclose(f);

    // Clean

    CHECK (cudaDeviceReset());

    return 0;
}
