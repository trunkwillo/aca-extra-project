// Compile using: nvcc -arch=sm_61 --compiler-options -O2,-Wall -I/usr/local/cuda/include main.cu -o teste
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <cuda.h>

void computeNewImage(int w, int h, int struct_size, int round, int s, unsigned int *img, unsigned int *final_image, float *weights){

    if(idx >= h || idy >= w){ // Fora do array
      return;
    }


    unsigned int *se = new unsigned int[struct_size];

    unsigned int temp_gray, temp_r, temp_g, temp_b, temp_value;
    int x, y, temp_pos;

    for(int i = 0; i < struct_size; i++){
      //gray(x, y) = 0.299 × red(x, y) + 0.587 × green(x, y) + 0.114 × blue(x, y)

      // Calcular posição 2D em array 1D do ponto do elemento estruturante que nos interessa
      x = i / s + (idx - (s/2));

      if(x < 0){
        x = -x;
      }
      if(x >= h){
        x = 2 * h - 2 - x;
      }

      y = i % s + (idy - (s / 2));
      if(y < 0){
        y = -y;
      }
      if(y >= w){
        y = 2 * w - 2 - y;
      }

      temp_pos = x * h + y;

      // Pegar nos valores todos e calcular o grey para todos os indices
      temp_r = (img[temp_pos] & 0x000000FF);
      temp_g = (img[temp_pos] & 0x0000FF00) >> 8;
      temp_b = (img[temp_pos] & 0x00FF0000) >> 16;

      temp_gray = (0.299 * temp_r) + (0.587 * temp_g) + (0.114 * temp_b);
      se[i] = img[temp_pos];
      se[i] = se[i] | (temp_gray << 24);
    }

    // if(idx == 0 && idy == 0){
    //   printf("KRNL: %u\n",se[0]);
    // }

    // Retirar cantos caso necessário (de maneira a não estragar a mediana e na média ponderada os pesos já vêem a 0)
    if(round){
      //printf("Não é suposto estar aqui para já");
      se[0] = 0x00000000;
      se[s - 1] = 0x00000000;
      se[struct_size - 1] = 0xFFFFFFFF;
      se[struct_size - s] = 0xFFFFFFFF;
    }

    // Ordenar o array
    unsigned int t_swap;
    for(int i = 0; i < struct_size; i++){
      // Last i elements are already in place
      for(int j = 0; j < struct_size; j++){
        if (se[j] > se[j+1]){
          t_swap = se[j];
          se[j] = se[i];
          se[i] = t_swap;
        }
      }
    }

    // if(idx == 0 && idy == 0){
    //   printf("KRNL: %u\n",se[0]);
    // }

    unsigned int new_r, new_g, new_b;
    // Multiplicar pelos pesos
    for(int b = 0; b < struct_size; b++){
      new_r = new_r + ((se[b] & 0x000000FF) * weights[b]);
      new_g = new_g + (((se[b] & 0x0000FF00) >> 8) * weights[b]);
      new_b = new_b + (((se[b] & 0x00FF0000) >> 16) * weights[b]);
    }


    // Armazenar na posição final
    final_image[idx * h + idy] = final_image[idx * h + idy] | new_r;
    final_image[idx * h + idy] = final_image[idx * h + idy] | (new_g << 8);
    final_image[idx * h + idy] = final_image[idx * h + idy] | (new_b << 16);

    // if(idx == 0 && idy == 0){
    //   printf("KRNL: %u\n",final_image[0]);
    // }
}

int main( int argc, char* argv[] ){
    unsigned int width, height, size, max_value; // Dimensões
    unsigned int reps = 5; // TODO: Define o nº de repetições no processo
    unsigned int struct_size;
    int rounded = 0;
    unsigned int *image;
    unsigned int *final_image;

    /*
        Lêr a imagem
    */
    FILE *stream = fopen("rninet_ascii.ppm", "r"); // TODO: Definir imagem de entrada
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;

    if (stream == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    // Magic number
    nread = getline(&line, &len, stream); // Must be "P3"
    if(strcmp(line, "P3\n") != 0){
      printf("Invalid file (Wrong magic number)\n");
      return 0;
    }


    //Dimensions
    nread = getline(&line, &len, stream);

    char *token = strtok(line, " ");
    width = atoi(token);
    token = strtok(NULL, " ");
    height = atoi(token);
    size = width * height;

    printf("Image size: %u x %u totalling %u\n",width, height, size);

    // Maximum value
    nread = getline(&line, &len, stream);
    max_value = atoi(line);

    /*
        Leitura dos valores e armazenamento
    */
    image = (unsigned int *) malloc(size * sizeof(unsigned int));
    for(int j = 0; j <= size * 3; j++){
      nread = getline(&line, &len, stream);
      if(atoi(line) > 255){
        printf("ERROR: Invalid value");
        return 0;
      }
      image[j/3] = image[j/3] | (atoi(line) << (8 * (j % 3)));
    }

    free(line);
    fclose(stream);

    /*
        Definir o elemento estruturante
    */
    int side = 3; // TODO: Definir o tamanho do lado do elemento estruturante
    struct_size = side * side;
    unsigned int real_size = struct_size;
    rounded = 0;      // TODO: Definir se tem cantos
    if(rounded){
      real_size -= 4;
    }
    int p = 12;       // TODO: Definir a relação
    if(p < real_size){
      printf("ERROR: Invalid relation");
      return 0;
    }

    // Constuir os 2 arrays do elemento estruturante
    float weight_map[struct_size]; // Definir o valor de todos
    for(int i = 0; i < struct_size; i++){
      weight_map[i] = (1.0/p);
    }
    weight_map[real_size / 2] = (p - ((real_size - 1)/ p));  // Definir como maior o valor do meio

    if(real_size == 21){ // Se o filtro não considerar os cantos basta colocar os seus pesos a 0
      weight_map[0] = 0;
      weight_map[4] = 0;
      weight_map[20] = 0;
      weight_map[25] = 0;
    }


    /*
        Enviar informação para o device
    */
    // Alocação
    unsigned int *d_image, *d_final_image; // Arrays com size elementos
    // int *d_width, *d_height, *d_struct_size, *d_round, *d_side;
    float *d_weight_map; // Array com struct_size elementos

    // cudaMalloc((void **)&d_width, sizeof(int)); // Comprimento
    // cudaMalloc((void **)&d_height, sizeof(int)); // Altura
    // cudaMalloc((void **)&d_struct_size, sizeof(int)); // Tamanho do elemento estruturante
    // cudaMalloc((void **)&d_round, sizeof(int)); // Cantos ou não
    // cudaMalloc((void **)&d_side, sizeof(int)); // Lado (podia calcular no device, mas em vez de calcular HEIGHT x WIDTH vezes envio já)

    final_image = (unsigned int *) malloc(size * sizeof(unsigned int));

    // Chamar kernel para calcular novo valor de cada pixel
    dim3 gridSize((int)(height/32)+1,(int)(width/32)+1); // Fazer a mais e mandar retornar os fora dos limites
    dim3 blockSize(32,32); // Aproveitar o máximo de threads permitidas para partilharem todas da mesma memória TODO: Verificar se este é o máximo
    printf("1: %u\n",image[0]);
    computeNewImage<<<gridSize,blockSize>>>(width,height, struct_size, rounded, side, d_image, d_final_image, d_weight_map);

    /*
        Retornar informação do device e general clean up
    */
    //printf("3: %u\n",final_image[0]);
    //cudaFree(d_rounded); cudaFree(d_width); cudaFree(d_height); cudaFree(d_struct_size);

    /*
        Guardar imagem
    */
    FILE *f = fopen("rninet_final.ppm", "w"); // TODO: Definir imagem de saída
    if (f == NULL){
        printf("Error opening file!\n");
        exit(1);
    }

    // Write magic number
    fprintf(f,"P3\n");

    // Write dimensions
    fprintf(f,"%u %u\n", width, height);

    // Write maximum value
    fprintf(f,"%u\n", max_value);

    // Write RGB values
    unsigned int r;
    unsigned int g;
    unsigned int b;
    for(int z = 0; z < size; z++){
      r = final_image[z] & 0x000000FF;
      g = (final_image[z] & 0x0000FF00)>>8;
      b = (final_image[z] & 0x00FF0000)>>16;
      fprintf(f,"%u\n%u\n%u\n",r,g,b);
    }
    fclose(f);

    // Clean

    CHECK (cudaDeviceReset ());

    return 0;
}
